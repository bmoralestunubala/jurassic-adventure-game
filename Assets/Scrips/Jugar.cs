﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Jugar : MonoBehaviour{
    //declaro un metodo para ser invocado dentro del Scrip
   public void iniciarJuego(){
       //utilizo un metodo para cargar la ecena de nivel.1
       SceneManager.LoadScene("Level_1");
   }
}
