﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TorosauruEncontrado : MonoBehaviour
{
    public Text mensajes;
    public GameObject BloqueoDominik;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            //la venta de las corruntinas es que se puede poner pequeños
            //temporizadores para controlar los momentos
            StartCoroutine("ChicaEncontrada");
        }
    }

    public IEnumerator ChicaEncontrada()
    {
        mensajes.text = "Por fin te encontré, ahora voy por Dominik";
        yield return new WaitForSeconds(2.0f);
        mensajes.text = "";
        Destroy(BloqueoDominik, 1.0f);
    }
}