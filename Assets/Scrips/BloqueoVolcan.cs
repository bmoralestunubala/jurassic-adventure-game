﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloqueoVolcan : MonoBehaviour
{
    public Text mensajes;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            mensajes.text = "Primero recoge el huevo de los Quetzalcoatlus, luego busca a Dominik";
        }
    }

    void OnTriggerExit(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            mensajes.text = "";
        }

    }
}
