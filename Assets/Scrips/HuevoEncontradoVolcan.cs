﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HuevoEncontradoVolcan : MonoBehaviour
{
    public Text mensajes;
    public GameObject BloqueoVolcan;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            //la venta de las corruntinas es que se puede poner pequeños
            //temporizadores para controlar los momentos
            StartCoroutine("ChicaEncontrada");
        }
    }

    public IEnumerator ChicaEncontrada()
    {
        mensajes.text = "Por fin te encontré";
        yield return new WaitForSeconds(2.0f);
        mensajes.text = "";
        Destroy(BloqueoVolcan, 1.0f);
    }
}