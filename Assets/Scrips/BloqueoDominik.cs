﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloqueoDominik : MonoBehaviour
{
    public Text mensajes;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            mensajes.text = "Encuentra el animal torosaurus, despues podras ver a Dominik";
        }
    }

    void OnTriggerExit(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            mensajes.text = "";
        }

    }
}
