﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HuevoEncontrado : MonoBehaviour
{
    public Text mensajes;
    public GameObject BloqueadorPuente;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            //la venta de las corruntinas es que se puede poner pequeños
            //temporizadores para controlar los momentos
            StartCoroutine("ChicaEncontrada");
        }
    }

    public IEnumerator ChicaEncontrada()
    {
        mensajes.text = "Por fin te encontré ahora tengo que buscar los demas huevos !!!";
        yield return new WaitForSeconds(2.0f);
        mensajes.text = "";
        Destroy(BloqueadorPuente, 1.0f);
    }
}