﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class GameController : MonoBehaviour{
 
    public int numeroHuevos;
    
    public Text textHuevos;
  
    // Start is called before the first frame update
    void Start()
    {
        
    }
 
    public void publicarColecciones(){
        //Debug. Log permite visualizar texto en la consola
        // se utiliza mucho para prototipar y monitorear
        Debug.Log("numeroHuevos -->>>" + numeroHuevos);
       
        textHuevos.text="numeroHuevos:" + numeroHuevos;
        
    }
 
    // Update is called once per frame
    void Update()
    {
        
    }
}