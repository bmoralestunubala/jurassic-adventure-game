﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arbolCaido : MonoBehaviour
{
   
    public GameObject arbol;
     Animator arbol_anim;
    void Start()
    {
        
        arbol_anim = arbol.GetComponent<Animator>();
    }

    void Update()
    {
        
    }
    void OnTriggerEnter(Collider Persona)
    {
        if(Persona.tag == "ManoDerecha")
        {
            arbol_anim.SetTrigger("caer_arbol");
        }
    }
}
