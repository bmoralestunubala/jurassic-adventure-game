﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloqueadorPuente : MonoBehaviour
{
    public Text mensajes;
    void OnTriggerEnter(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            mensajes.text = "Primero encuentra los dos huevos";
        }
    }

    void OnTriggerExit(Collider objeto)
    {
        if (objeto.tag == "Player")
        {
            mensajes.text = "";
        }

    }
}
