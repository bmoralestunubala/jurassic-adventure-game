**---[JURASSIC ADVENTURE GAME ]---**
Es un proyecto que nació en el área de Electiva II del octavo semestre, este es un juego educativo que surgió por medio de una serie de lluvia de ideas, de donde se realizaron preguntas como. ¿Qué tipo de juego le gustaría que hiciera?, del cual  seleccionamos una sola respuesta. 

El **video** del funcionamiento se puede ver en el siguiente enlace:

https://www.youtube.com/watch?v=sYtkkGc9c0I


Este juego tiene como objetivo brindar conocimiento de la paleontología de una manera divertida, donde el usuario deberá recoger la mayor cantidad de huevos de dinosaurios, ademas deberá recorrer montañas muy inclinadas y pasar por medio de un volcán.

![fps](/Docs/dino1.png?raw=true "fps")

![fps](/Docs/dino2.png?raw=true "fps")

![fps](/Docs/dino3.png?raw=true "fps")

El usuario deberá tener en cuenta la siguiente información para el manejo del juego:
- Con las flechas tiene movimiento o con el teclado W
- Con la letra G puede recoger los objetos 
- Con Click izquierdo podemos efectuar una patada
- Con Click derecho efectuamos un golpe de puño
- Con barra espaciadora efectúa un salto

El **Game concept Document**  se observa en el siguiente enlace:

https://gitlab.com/bmoralestunubala/jurassic-adventure-game/-/blob/master/Docs/Game%20concept%20Document.pdf
